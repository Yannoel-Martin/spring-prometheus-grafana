Feature: Create Product From Api

  Background:
    * url 'http://localhost:8080'

  Scenario: Fetch random quote

    Given path 'api/product'
    When method POST
    Then status 200
    And match $ == {'#notnull'}