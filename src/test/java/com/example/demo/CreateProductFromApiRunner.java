package com.example.demo;

import com.intuit.karate.junit5.Karate;

public class CreateProductFromApiRunner {
    @Karate.Test
    Karate testCreateProductFromApi() {
        return Karate.run("api/product").relativeTo(getClass());
    }
}
